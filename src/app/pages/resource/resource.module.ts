import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ResourceSelectionComponent } from './selection/resource-selection.component';
import { ResourceConfirmationComponent } from './confirmation/resource-confirmation.component';
import { ResourcePageComponent } from './resource-page/resource-page.component';
import { resourceRoutes } from './resource.routes';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [
    ResourceSelectionComponent,
    ResourceConfirmationComponent,
    ResourcePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(resourceRoutes),
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatDividerModule
  ]
})
export class ResourceModule { }
