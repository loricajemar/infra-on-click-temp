import { Routes } from "@angular/router";
import { ResourcePageComponent } from "./resource-page/resource-page.component";

export const resourceRoutes: Routes = [
    { path: 'resource-page', component: ResourcePageComponent }
]