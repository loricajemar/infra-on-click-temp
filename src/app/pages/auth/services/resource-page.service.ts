import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})

export class ResourcePageService {

    constructor(){ }


    form: FormGroup = new FormGroup({
        cloudPlatform: new FormControl('', [Validators.required]),
        resourceGroupName: new FormControl('',[Validators.required]),
        location: new FormControl('',[Validators.required]),
        virtualNetworkAddressSpace: new FormControl({value: '10.0.0.0/16', disabled: true}),
        subnetAddressPrefix: new FormControl({value: '10.0.2.0/24', disabled: true}),
        ipAddressAllocation: new FormControl({value: 'Dynamic', disabled: true}),
    });
}
