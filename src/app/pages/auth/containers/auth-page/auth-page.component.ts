import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { routes } from 'src/app/consts/routes';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit {
  routers: typeof routes = routes

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  submitSignupForm() {
    this.authService.signup()
    this.router.navigate([this.routers.RESOURCE_PAGE]).then()
  }

  submitLoginForm() {
    this.authService.login()
    this.router.navigate([this.routers.RESOURCE_PAGE]).then()
  }

  ngOnInit(): void {
  }

}
