import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() submitLoginForm = new EventEmitter<void>()
  form: FormGroup
  iocEmail = 'jim.lorica@fisglobal.com' //placeholder only
  iocPassword = 'admin' //placeholder only

  constructor() { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(this.iocEmail, [Validators.required, Validators.email]),
      password: new FormControl(this.iocPassword, [Validators.required])
    })
  }

  login() {
    if (this.form.valid) {
      this.submitLoginForm.emit()
    }
  }

}
